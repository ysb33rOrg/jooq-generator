/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq;

import org.ysb33r.jooq.internal.DatabaseContainerLoader;

import java.util.List;

public interface DatabaseContainer {
    static DatabaseContainer createContainer(
            DatabaseTypes databaseType,
            String databaseVersion,
            String databaseName
    ) {
        return DatabaseContainerLoader.load(databaseType, databaseVersion, databaseName);
    }

    String getDriver();

    String getUrl();

    String getName();

    String getJooqDriver();

    String getUsername();

    String getPassword();

    List<String> getExcludes();

    void startContainer();

    void stopContainer();
}
