/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq.internal

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import liquibase.integration.commandline.Main
import org.ysb33r.jooq.DatabaseContainer
import org.ysb33r.jooq.LiquibaseProvider

/**
 * Implements the default version of providing a liquibase runner
 */
@CompileStatic
@Slf4j
class DefaultLiquibaseProvider implements LiquibaseProvider {
    @SuppressWarnings('UnnecessaryCast')
    DefaultLiquibaseProvider(
        DatabaseContainer database,
        String pathToChangeLog,
        List<File> databaseChangeLogClasspaths,
        Optional<File> tmpDir
    ) {
        emptyPropsFile = new File(
            tmpDir.orElse( new File(System.getProperty('java.io.tmpdir'))),
            'empty-liquibase.properties'
        )
        emptyPropsFile.deleteOnExit()
        runParameters = [
            "--changeLogFile=${pathToChangeLog}",
            "--url=${database.url}",
            "--driver=${database.driver}",
            '--logLevel=info',
            "--defaultsFile=${emptyPropsFile}",
            "--username=${database.username}",
            "--password=${database.password}"
        ] as List<String>

        if (databaseChangeLogClasspaths) {
            runParameters.add(
                "--classpath=${databaseChangeLogClasspaths*.absolutePath.join(File.pathSeparator)}".toString()
            )
        }

        log.info("Starting Liquibase with ${runParameters} in ${new File('.').absolutePath}")
        runParameters.add('update')
    }

    void createSchema() {
        emptyPropsFile.parentFile.mkdirs()
        emptyPropsFile.text = ''
        new Main().run(runParameters.toArray() as String[])
    }

    private final File emptyPropsFile
    private final List<String> runParameters
}
