/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq.internal

import groovy.transform.CompileStatic
import org.ysb33r.jooq.DatabaseContainer
import org.ysb33r.jooq.DatabaseTypes
import org.ysb33r.jooq.NotImplementedException
import org.ysb33r.jooq.dbcontainer.mysql.MySqlDatabaseContainer

import static org.ysb33r.jooq.DatabaseTypes.MYSQL

/**
 * Loads a container depending on the type of database.
 */
@CompileStatic
class DatabaseContainerLoader {
    static DatabaseContainer load(DatabaseTypes databaseType, String databaseVersion, String databaseName) {
        switch (databaseType) {
            case MYSQL:
                return new MySqlDatabaseContainer(databaseVersion, databaseName)
            default:
                throw new NotImplementedException("${databaseType} not fully supported as yet")
        }
    }
}
