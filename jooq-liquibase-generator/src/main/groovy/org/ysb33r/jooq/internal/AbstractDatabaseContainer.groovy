/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq.internal

import groovy.transform.CompileStatic
import org.apache.commons.lang3.RandomStringUtils
import org.testcontainers.containers.JdbcDatabaseContainer
import org.ysb33r.jooq.DatabaseContainer
import org.ysb33r.jooq.DatabaseTypes

/**
 * Testcontainers wrapper for a specific database
 */
@CompileStatic
abstract class AbstractDatabaseContainer implements DatabaseContainer {

    final String name
    final List<String> excludes = []

    @Override
    String getUrl() {
        if (!databaseContainer) {
            startContainer()
        }
        databaseContainer.jdbcUrl
    }

    @Override
    String getJooqDriver() {
        dbType.jooqDriverName
    }

    @Override
    void startContainer() {
        if (!databaseContainer) {
            createContainer()
        }
        databaseContainer.start()
    }

    @Override
    void stopContainer() {
        databaseContainer?.stop()
    }

    abstract void createContainer()

    protected AbstractDatabaseContainer(DatabaseTypes databaseType, String databaseVersion, String databaseName) {
        this.dbType = databaseType
        name = databaseName
        this.containerVersion = databaseVersion
    }

    protected JdbcDatabaseContainer<?> databaseContainer
    protected final DatabaseTypes dbType
    protected final String containerVersion

    protected String generatePassword(int length) {
        RandomStringUtils.random(length, true, true)
    }
}
