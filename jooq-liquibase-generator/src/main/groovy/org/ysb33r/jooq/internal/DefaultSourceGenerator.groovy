/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq.internal

import groovy.transform.CompileStatic
import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.Configuration
import org.jooq.meta.jaxb.Database
import org.jooq.meta.jaxb.Generate
import org.jooq.meta.jaxb.Generator
import org.jooq.meta.jaxb.Jdbc
import org.jooq.meta.jaxb.Strategy
import org.jooq.meta.jaxb.Target
import org.ysb33r.jooq.DatabaseContainer
import org.ysb33r.jooq.LiquibaseProvider
import org.ysb33r.jooq.SourceGenerator

/**
 * Implementation of a Java source generator.
 */
@CompileStatic
class DefaultSourceGenerator implements SourceGenerator {

    private static final String NO_LIQUIBASE_TABLES = 'DATABASECHANGELOG(LOCK)?'
    private static final String BAR = '|'

    final Database database
    final Strategy strategy
    final Target target

    DefaultSourceGenerator(DatabaseContainer dbc) {
        final excludes = ([NO_LIQUIBASE_TABLES] + dbc.excludes).join(BAR)
        jdbc = new Jdbc()
            .withUrl(dbc.url)
            .withDriver(dbc.driver)
            .withPassword(dbc.password)
            .withUsername(dbc.username)
        database = new Database()
            .withName(dbc.jooqDriver)
            .withExcludes(excludes)
            .withInputSchema(dbc.name)

        strategy = new Strategy()
        generate = new Generate()
        target = new Target()
    }

    DefaultSourceGenerator(DatabaseContainer dbc, Generate options) {
        final excludes = ([NO_LIQUIBASE_TABLES] + dbc.excludes).join(BAR)
        jdbc = new Jdbc()
            .withUrl(dbc.url)
            .withDriver(dbc.driver)
            .withPassword(dbc.password)
            .withUsername(dbc.username)
        database = new Database()
            .withName(dbc.jooqDriver)
            .withExcludes(excludes)
            .withInputSchema(dbc.name)
            .withOutputSchemaToDefault(true)
        strategy = new Strategy()
        generate = options
        target = new Target()
    }

    @Override
    Generate getGenerateOptions() {
        this.generate
    }

    @Override
    void generateSource(
        LiquibaseProvider liquibase
    ) {
        Configuration jooqConfiguration = new Configuration()
            .withJdbc(jdbc)
            .withGenerator(new Generator()
                .withStrategy(strategy)
                .withDatabase(database)
                .withGenerate(generate)
                .withTarget(target)
            )
        liquibase.createSchema()
        GenerationTool.generate(jooqConfiguration)
    }

    private final Generate generate
    private final Jdbc jdbc
}
