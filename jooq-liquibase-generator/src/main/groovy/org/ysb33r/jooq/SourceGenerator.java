/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq;

import org.jooq.meta.jaxb.Database;
import org.jooq.meta.jaxb.Generate;
import org.jooq.meta.jaxb.Strategy;
import org.jooq.meta.jaxb.Target;
import org.ysb33r.jooq.internal.DefaultSourceGenerator;

public interface SourceGenerator {
    static SourceGenerator create(DatabaseContainer database) {
        return new DefaultSourceGenerator(database);
    }

    static SourceGenerator create(DatabaseContainer database, Generate generateOptions) {
        return new DefaultSourceGenerator(database, generateOptions);
    }

    Database getDatabase();

    Strategy getStrategy();

    Generate getGenerateOptions();

    Target getTarget();

    void generateSource(
            LiquibaseProvider liquibase
    );
}
