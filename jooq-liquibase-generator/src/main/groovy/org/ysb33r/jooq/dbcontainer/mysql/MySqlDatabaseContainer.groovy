/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq.dbcontainer.mysql

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.testcontainers.containers.JdbcDatabaseContainer
import org.testcontainers.containers.MySQLContainer
import org.testcontainers.containers.output.Slf4jLogConsumer
import org.testcontainers.utility.DockerImageName
import org.ysb33r.jooq.internal.AbstractDatabaseContainer

import static org.ysb33r.jooq.DatabaseTypes.MYSQL

/**
 * Knows how to start a Mysql container.
 */
@CompileStatic
@Slf4j
class MySqlDatabaseContainer extends AbstractDatabaseContainer {

    MySqlDatabaseContainer(String databaseVersion, String databaseName) {
        super(MYSQL, databaseVersion, databaseName)
    }

    final String username = 'root'

    @Override
    String getDriver() {
        dbType.jdbcDriverName(containerVersion)
    }

    @Override
    String getPassword() {
        dbPassword
    }

    @Override
    void createContainer() {
        dbPassword = generatePassword(12)
        final imageName = DockerImageName.parse(dbType.containerImageName(containerVersion))
            .asCompatibleSubstituteFor('mysql')
        this.databaseContainer = new MySQLContainer(imageName)
            .withDatabaseName(name)
            .withUsername('jooq_user')
            .withPassword(dbPassword)
            .withEnv('MYSQL_ROOT_PASSWORD', dbPassword)
            .withEnv('MYSQL_ROOT_HOST', '%')
            .withLogConsumer(new Slf4jLogConsumer(log)) as JdbcDatabaseContainer
    }

    private String dbPassword
}
