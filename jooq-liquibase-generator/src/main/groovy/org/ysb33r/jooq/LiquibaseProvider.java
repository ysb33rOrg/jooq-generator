/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq;

import org.ysb33r.jooq.internal.DefaultLiquibaseProvider;

import java.io.File;
import java.util.List;
import java.util.Optional;

public interface LiquibaseProvider {
    static LiquibaseProvider createProvider(
            DatabaseContainer database,
            String pathToChangeLog,
            List<File> databaseChangeLogClasspaths,
            Optional<File> tmpDir
    ) {
        return new DefaultLiquibaseProvider(database,  pathToChangeLog, databaseChangeLogClasspaths, tmpDir);
    }

    void createSchema();
}
