/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq

import groovy.transform.CompileStatic
import org.ysb33r.grolifant.api.core.OperatingSystem

import java.util.function.Function

import static org.ysb33r.jooq.internal.Functions.DEFAULT_GAV

@CompileStatic
enum DatabaseTypes {

    MYSQL(
        'mysql',
        'org.jooq.meta.mysql.MySQLDatabase',
        { String v ->
            'com.mysql.cj.jdbc.Driver'
        },
        DEFAULT_GAV.curry('mysql:mysql-connector-java'),
        { String v ->
            if (v.startsWith('5.') &&
                OperatingSystem.current().macOsX &&
                OperatingSystem.current().arch == OperatingSystem.Arch.ARM64) {
                // TODO: Might need to fix this for Apple M1/M2
                "mysql/mysql-server:${v}"
            } else {
                "mysql/mysql-server:${v}"
            }
        }
    )

    /*
    MariaDB (latest versions)
    PostgreSQL (latest versions)
     */

    final String identifier
    final String jooqDriverName

    String jdbcDriverName(String version) {
        this.jdbcDriver.apply(version)
    }

    String jdbcGav(String version) {
        this.jdbcGav.apply(version)
    }

    String containerImageName(String versionTag) {
        this.containerImage.apply(versionTag)
    }

    String testContainersGav(String version) {
        "org.testcontainers:${identifier}:${version}"
    }

    private DatabaseTypes(
        String identifier,
        String jooqDriver,
        Function<String, String> jdbcDriver,
        Function<String, String> jdbcGav,
        Function<String, String> containerImage
    ) {
        this.identifier = identifier
        this.jooqDriverName = jooqDriver
        this.jdbcDriver = jdbcDriver
        this.jdbcGav = jdbcGav
        this.containerImage = containerImage
    }

    private final Function<String, String> jdbcDriver
    private final Function<String, String> jdbcGav
    private final Function<String, String> containerImage
}