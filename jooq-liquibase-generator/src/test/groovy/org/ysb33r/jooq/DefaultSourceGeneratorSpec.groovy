/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.jooq

import spock.lang.Specification

class DefaultSourceGeneratorSpec extends Specification {
    public static final File CHANGELOGS = new File(System.getProperty('CHANGELOG_LOCATION'))

    File root

    void setup() {
        root = File.createTempDir()
    }

    void cleanup() {
        root.deleteDir()
    }

    void 'Generate code from changelogs'() {
        setup:
        final db = DatabaseContainer.createContainer(DatabaseTypes.MYSQL, '5.7.30', 'test')

        db.startContainer()
        final lb = LiquibaseProvider.createProvider(
            db,
            'changelog.yml',
            [CHANGELOGS],
            Optional.of(root)
        )
        final sg = SourceGenerator.create(db)
        sg.target.withDirectory(root.absolutePath)
        sg.target.withPackageName('example.test')

        when:
        sg.generateSource(lb)

        then:
        new File(root, 'example/test/tables/TestTable.java').exists()
        !new File(root, 'example/test/tables/Databasechangeloglock.java').exists()

        cleanup:
        db?.stopContainer()
    }
}