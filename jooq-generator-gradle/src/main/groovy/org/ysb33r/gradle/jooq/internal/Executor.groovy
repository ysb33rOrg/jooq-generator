/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.jooq.internal

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.workers.WorkAction
import org.ysb33r.jooq.DatabaseContainer
import org.ysb33r.jooq.LiquibaseProvider
import org.ysb33r.jooq.SourceGenerator

/**
 * Executes the conversion in a worker.
 */
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
@CompileStatic
@Slf4j
abstract class Executor implements WorkAction<Parameters> {

    @Override
    void execute() {
        def db = DatabaseContainer.createContainer(
            parameters.databaseType.get(),
            parameters.databaseVersion.get(),
            parameters.databaseName.get()
        )
        db.excludes.addAll(parameters.excludes.get())

        def lb = LiquibaseProvider.createProvider(
            db,
            parameters.databaseChangelog.get(),
            [parameters.databaseChangelogClasspath.get()],
            Optional.of(parameters.tmpDir.get())
        )
        def sg = SourceGenerator.create(db, parameters.generateOptions.get())
        sg.target.withDirectory(parameters.outputDirectory.get().absolutePath)
        sg.target.withPackageName(parameters.packageName.get())
        sg.generateSource(lb)
        db.stopContainer()
    }
}
