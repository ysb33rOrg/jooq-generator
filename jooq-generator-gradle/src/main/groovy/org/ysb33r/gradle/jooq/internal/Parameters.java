/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.jooq.internal;

import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.Property;
import org.gradle.workers.WorkParameters;
import org.jooq.meta.jaxb.Generate;
import org.ysb33r.jooq.DatabaseTypes;

import java.io.File;
import java.util.List;

public interface Parameters extends WorkParameters {
     Property<DatabaseTypes> getDatabaseType();

     Property<String> getDatabaseVersion();

     Property<String> getDatabaseName();

     Property<String> getDatabaseChangelog();

     Property<File> getDatabaseChangelogClasspath();

     Property<File> getOutputDirectory();

     Property<String> getPackageName();

     Property<File> getTmpDir();

     Property<Generate> getGenerateOptions();

     ListProperty<String> getExcludes();
}
