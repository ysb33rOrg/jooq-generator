/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.jooq;

import groovy.lang.Closure;
import org.gradle.api.Action;
import org.jooq.meta.Database;
import org.jooq.meta.jaxb.Generate;
import org.ysb33r.grolifant.api.core.ClosureUtils;

import java.io.Serializable;
import java.util.List;

public interface DatabaseSpec extends Serializable {
    void setJdbcVersion(String ver);
    String getJdbcVersion();
    void setDatabaseVersion(String ver);
    String getDatabaseVersion();
    void setAlias(String alias);
    String getAlias();

    Generate getGenerateOptions();

    List<String> getExcludes();

    default void generateOptions( Action<Generate> configurator) {
        configurator.execute(getGenerateOptions());
    }

    default void generateOptions(Closure configurator) {
        ClosureUtils.configureItem(getGenerateOptions(), configurator);
    }
}
