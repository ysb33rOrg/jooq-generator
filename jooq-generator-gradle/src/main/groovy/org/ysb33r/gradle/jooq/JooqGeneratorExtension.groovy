/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.jooq

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.DependencyResolveDetails
import org.gradle.api.artifacts.DependencySet
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.jooq.internal.DefaultDatabaseSpec
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.jooq.DatabaseTypes

import java.util.concurrent.Callable
import java.util.function.Function

import static org.ysb33r.grolifant.api.core.ResourceUtils.loadPropertiesFromResource

/**
 * Extension for configuring the defaults for a source code generator.
 */
@CompileStatic
class JooqGeneratorExtension {

    private static final String CONFIGURATION_NAME = '___J00Q_GENER@T0R_INTERN@L___'

    @SuppressWarnings('DuplicateStringLiteral')
    JooqGeneratorExtension(Project project) {
        this.projectOperations = ProjectOperations.find(project)
        this.versions = loadVersions(projectOperations)
        this.databaseTypes = [:]

        this.databaseTypesProvider = project.provider({ Map<DatabaseTypes, DatabaseSpec> x -> x }
            .curry(this.databaseTypes) as Callable<Map<DatabaseTypes, DatabaseSpec>>)

        this.createDep = {
            DependencyHandler dep, String gav ->
                dep.create(gav)
        }.curry(project.dependencies) as Function<String, Dependency>

        this.configuration = project.configurations.create(CONFIGURATION_NAME)
        this.configuration.visible = false
        this.configuration.canBeConsumed = false
        this.configuration.canBeResolved = true
        this.configuration.defaultDependencies(new Action<DependencySet>() {
            @Override
            void execute(DependencySet deps) {
                deps.addAll(dependencies)
            }
        })

        this.configuration.resolutionStrategy.eachDependency(new Action<DependencyResolveDetails>() {
            @Override
            void execute(DependencyResolveDetails drd) {
                if (drd.requested.group == 'org.testcontainers' && !drd.requested.version) {
                    drd.useVersion(testContainersVersion)
                }
                if (drd.requested.group == 'org.jooq' && drd.requested.name == 'jooq-codegen' &&
                    !drd.requested.version) {
                    drd.useVersion(jooqVersion)
                }
                if (drd.requested.group == 'org.liquibase' && drd.requested.name == 'liquibase-core' &&
                    !drd.requested.version) {
                    drd.useVersion(liquibaseVersion)
                }
                if (drd.requested.group == 'org.liquibase' && drd.requested.name == 'liquibase-groovy-dsl' &&
                    !drd.requested.version) {
                    drd.useVersion(versions['liquibase.groovy'])
                }
                if (drd.requested.group == 'org.slf4j' &&
                    !drd.requested.version) {
                    drd.useVersion(versions['slf4j'])
                }
                if (drd.requested.group == 'org.yaml' && drd.requested.name == 'snakeyaml' &&
                    !drd.requested.version) {
                    drd.useVersion(versions['snakeyaml'])
                }
            }
        })

        dependencies.add(createDep.apply('org.jooq:jooq-codegen:'))
        dependencies.add(createDep.apply('org.liquibase:liquibase-core:'))
        dependencies.add(createDep.apply('org.slf4j:slf4j-simple:'))

        this.jooqVersion = versions['jooq']
        this.testContainersVersion = versions['testcontainers']
        this.liquibaseVersion = versions['liquibase']
    }

    DatabaseSpec forDatabase(String db) {
        forDatabase(DatabaseTypes.valueOf(db.toUpperCase(Locale.US)))
    }

    DatabaseSpec forDatabase(DatabaseTypes db) {
        forDatabase(db) { it -> }
    }

    DatabaseSpec forDatabase(String db, Action<DatabaseSpec> dbSpec) {
        forDatabase(DatabaseTypes.valueOf(db.toUpperCase(Locale.US)), dbSpec)
    }

    DatabaseSpec forDatabase(DatabaseTypes db, Action<DatabaseSpec> dbSpec) {
        DefaultDatabaseSpec configuration = new DefaultDatabaseSpec(
            jdbcVersion: versions["jdbc.${db.identifier}".toString()],
            databaseVersion: versions["database.${db.identifier}".toString()],
            alias: db.identifier
        )

        dbSpec.execute(configuration)

        this.databaseTypes.put(db, configuration)
        dependencies.add(createDep.apply(db.jdbcGav(configuration.jdbcVersion)))
        dependencies.add(createDep.apply(db.testContainersGav('')))
        configuration
    }

    FileCollection getClasspath() {
        this.configuration
    }

    String getTestContainersVersion() {
        this.testContainersVersion
    }

    String getJooqVersion() {
        this.jooqVersion
    }

    String getLiquibaseVersion() {
        this.liquibaseVersion
    }

    Provider<Map<DatabaseTypes, DatabaseSpec>> getTargetDatabases() {
        this.databaseTypesProvider
    }

    void yamlParser() {
        dependencies.add(createDep.apply('org.yaml:snakeyaml:'))
    }

    void groovyParser() {
        dependencies.add(createDep.apply('org.liquibase:liquibase-groovy-dsl:'))
    }

    void jsonParser() {
        yamlParser()
    }

    private static Map<String, String> loadVersions(ProjectOperations po) {
        final String resourcePath = 'jooq-generator-gradle/versions.properties'

        Properties props = loadPropertiesFromResource(resourcePath)
        props.collectEntries {
            [po.stringTools.stringize(it.key), po.stringTools.stringize(it.value)]
        } as Map<String, String>
    }

    private final String testContainersVersion
    private final String jooqVersion
    private final String liquibaseVersion
    private final Map<String, String> versions
    private final Function<String, Dependency> createDep
    private final ProjectOperations projectOperations
    private final Set<Dependency> dependencies = []
    private final Configuration configuration
    private final Map<DatabaseTypes, DatabaseSpec> databaseTypes
    private final Provider<Map<DatabaseTypes, DatabaseSpec>> databaseTypesProvider
}
