/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.jooq

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.grolifant.api.core.ProjectOperations

/**
 * Plugin for running a source code generator.
 */
@CompileStatic
class JooqGeneratorPlugin implements Plugin<Project> {
    public static final String EXTENSION_NAME = 'jooqGeneration'
    public static final String DEFAULT_GENERATOR_TASK = 'jooqSourceGenerator'

    @Override
    @SuppressWarnings('UnnecessaryObjectReferences')
    void apply(Project project) {
        ProjectOperations po = ProjectOperations.maybeCreateExtension(project)
        JooqGeneratorExtension jooqGenerator = project.extensions.create(
            EXTENSION_NAME,
            JooqGeneratorExtension,
            project
        )
        project.tasks.register(DEFAULT_GENERATOR_TASK, JooqGenerator, new Action<JooqGenerator>() {
            @Override
            void execute(JooqGenerator t) {
                t.group = 'Code generation'
                t.description = 'Generates Java source code'
                t.classpath = jooqGenerator.classpath
                t.targetDatabases = jooqGenerator.targetDatabases
                t.outputDirectory = po.buildDirDescendant('jooq')
                t.changesetDirectory = './src/db'
                t.changelog = t.changesetDirectory.map { new File(it, 'changelog.xml') }
            }
        })
    }
}
