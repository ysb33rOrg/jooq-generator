/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.jooq

import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.SkipWhenEmpty
import org.gradle.api.tasks.TaskAction
import org.gradle.workers.WorkQueue
import org.gradle.workers.WorkerExecutor
import org.ysb33r.gradle.jooq.internal.Executor
import org.ysb33r.gradle.jooq.internal.Parameters
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.jooq.DatabaseTypes

import javax.inject.Inject

/**
 * Task for running a source code generator
 */
@CompileStatic
class JooqGenerator extends DefaultTask {
    @Inject
    JooqGenerator(WorkerExecutor we) {
        this.workerExecutor = we
        this.projectOperations = ProjectOperations.find(project)
        this.prefix = project.objects.property(String)
        this.outputDirectory = project.objects.property(File)
        this.changesetDirectory = project.objects.property(File)
        this.changelog = project.objects.property(String)
        this.workDir = projectOperations.buildDirDescendant("tmp/${name}")
        this.projectDir = project.projectDir
    }

    @InputFiles
    FileCollection getClasspath() {
        this.jooqClasspath
    }

    void setClasspath(FileCollection cfg) {
        this.jooqClasspath = cfg
    }

    @InputDirectory
    @SkipWhenEmpty
    Provider<File> getChangesetDirectory() {
        this.changesetDirectory
    }

    void setChangesetDirectory(Object dir) {
        projectOperations.fsOperations.updateFileProperty(this.changesetDirectory, dir)
    }

    @Input
    Provider<String> getChangelog() {
        this.changelog
    }

    void setChangelog(Object filePath) {
        projectOperations.stringTools.updateStringProperty(this.changelog, filePath)
    }

    @Input
    Provider<Map<DatabaseTypes, DatabaseSpec>> getTargetDatabases() {
        this.targets
    }

    void setTargetDatabases(Provider<Map<DatabaseTypes, DatabaseSpec>> targets) {
        this.targets = targets
    }

    @OutputDirectory
    Provider<File> getOutputDirectory() {
        this.outputDirectory
    }

    void setOutputDirectory(Object dir) {
        projectOperations.fsOperations.updateFileProperty(this.outputDirectory, dir)
    }

    @Input
    Provider<String> getPackageNamePrefix() {
        prefix
    }

    void setPackageNamePrefix(Object prefix) {
        projectOperations.stringTools.updateStringProperty(this.prefix, prefix)
    }

    @TaskAction
    void exec() {
        WorkQueue work = workerExecutor.processIsolation {
            it.classpath.from(classpath)
            it.forkOptions.workingDir(projectDir)
            it.forkOptions.environment(System.getenv())
        }

        final targetDir = this.outputDirectory.get()

        targetDatabases.get().each { DatabaseTypes db, DatabaseSpec spec ->
            work.submit(Executor) { Parameters params ->
                params.with {
                    databaseType.set(db)
                    databaseName.set('test')
                    databaseVersion.set(spec.databaseVersion)
                    databaseChangelog.set(changelog.get())
                    databaseChangelogClasspath.set(changesetDirectory.get())
                    packageName.set("${packageNamePrefix.get()}.${spec.alias}".toString())
                    tmpDir.set(workDir.get())
                    generateOptions.set(spec.generateOptions)
                    excludes.set(spec.excludes)
                }
                params.outputDirectory.set(new File(targetDir, db.identifier))
            }
        }

        work.await()
    }

    private final WorkerExecutor workerExecutor
    private final ProjectOperations projectOperations
    private final Property<File> outputDirectory
    private final Property<File> changesetDirectory
    private final Property<String> changelog
    private final Property<String> prefix
    private final Provider<File> workDir
    private final File projectDir
    private FileCollection jooqClasspath
    private Provider<Map<DatabaseTypes, DatabaseSpec>> targets
}
