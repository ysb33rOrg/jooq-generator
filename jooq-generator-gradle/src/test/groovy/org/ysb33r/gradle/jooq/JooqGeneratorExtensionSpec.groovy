/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.jooq

import org.gradle.api.file.FileCollection
import org.ysb33r.gradle.jooq.internal.UnitSpecification

class JooqGeneratorExtensionSpec extends UnitSpecification {

    void 'Resolve dependencies'() {
        setup:
        project.allprojects {
            repositories {
                jcenter()
            }

            jooqGeneration {
                forDatabase 'mysql'
            }
        }

        FileCollection config = project.jooqGeneration.classpath

        when:
        def files = config.files*.name

        then:

        files.find { it.startsWith('mysql-connector-j') }
        files.find { it.startsWith('jooq-codegen') }
        files.find { it.startsWith('testcontainers') }
        files.find { it.startsWith('liquibase') }
    }
}
