/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.jooq.internal

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.ysb33r.grolifant.api.core.OperatingSystem
import spock.lang.Specification

class IntegrationSpecification extends Specification {
    public final static OperatingSystem OS = OperatingSystem.current()
    public static final String CHANGELOG_DIRECTORY = System.getProperty(
        'CHANGELOG_LOCATION',
        './build/resources/test/test-database'
    )

    @Rule
    TemporaryFolder testProjectDir

    File projectDir
    File testkitDir
    File buildFile
    File settingsFile

    void setup() {
        projectDir = new File(testProjectDir.root, 'test-project')
        testkitDir = new File(testProjectDir.root, 'testkit')
        buildFile = new File(projectDir, 'build.gradle')
        settingsFile = new File(projectDir, 'settings.gradle')

        projectDir.mkdirs()
        testkitDir.mkdirs()

//        LocalRepo.init(projectDir)

        settingsFile.text = ''
        buildFile.text = '''
        plugins {
            id 'org.ysb33r.jooq'
        }

        repositories {
            mavenCentral()
        }
        '''
    }

    GradleRunner getGradleRunner(List<String> taskNames ) {
        GradleRunner.create()
            .withProjectDir(projectDir)
            .withTestKitDir(testkitDir)
            .withArguments(taskNames + [ '-s', '-i'])
            .withPluginClasspath()
            .forwardOutput()
            .withDebug(true)
    }

    void copyChangelog() {
        def ant = new AntBuilder()
        ant.sequential {
            copy(todir: "${projectDir}/src/db") {
                fileset(dir: CHANGELOG_DIRECTORY) {
                    include(name: '**/*')
                }
            }
        }
    }
}