/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2020-2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.jooq

import org.ysb33r.gradle.jooq.internal.IntegrationSpecification

class JooqGeneratorSpec extends IntegrationSpecification {

    void 'Generate code'() {
        setup:
        copyChangelog()
        buildFile << """
        jooqGeneration {
            forDatabase 'mysql', {
                alias = 'mysql2'
                generateOptions {
                    jpaAnnotations = true
                }
            }
            yamlParser()
        }

        tasks.jooqSourceGenerator {
            changesetDirectory = 'src/db'
            changelog = 'changelog.yml'
            packageNamePrefix = 'integration.test'
        }
        """

        when:
        getGradleRunner(['jooqSourceGenerator']).build()

        then:
        new File(projectDir, 'build/jooq/mysql/integration/test/mysql2/Tables.java').exists()
        new File(projectDir, 'build/jooq/mysql/integration/test/mysql2/tables/records/TestTableRecord.java').text
            .contains('@Entity')
    }
}